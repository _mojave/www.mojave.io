<article <?php post_class("card mb-3"); ?>>
  <?php
    if(has_post_thumbnail()) {
      echo '<img class="card-img-top" src="http://lorempixel.com/400/200/nature" alt="Card image cap">';
    } else {
      echo '<div class="card-img-top bg-primary" style="height: 65px;"></div>';
    }
  ?>
  <div class="card-img-overlay">
    <?php
      foreach((get_the_category()) as $category) {
        echo '<a href="#" class="badge badge-secondary mr-1">' . $category->cat_name . '</a>';
      }
    ?>
  </div>
  <div class="card-body">
    <header>
      <h2 class="entry-title card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="entry-summary">
      <p class="card-text"><?php echo get_the_excerpt(); ?></p>
    </div>
    <?php get_template_part('templates/entry-meta'); ?>
    <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
  </div>
</article>
