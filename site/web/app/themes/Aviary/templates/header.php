<?php use Roots\Sage\Titles; ?>

<header class="banner">
  <nav class="navbar sticky-top navbar-expand-lg py-3 navbar-dark bg-primary gradient-primary">
    <div class="container">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#headerNavigation" aria-controls="headerNavigation" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand font-weight-bold" href="<?= esc_url(home_url('/')); ?>">
      <?php bloginfo('name'); ?>
    </a>
    <?php
      wp_nav_menu([
        'menu'            => 'header_navigation',
        'theme_location'  => 'header_navigation',
        'container'       => 'div',
        'container_id'    => 'headerNavigation',
        'container_class' => 'collapse navbar-collapse',
        'menu_id'         => false,
        'menu_class'      => 'navbar-nav ml-auto',
        'depth'           => 2,
        'fallback_cb'     => 'bs4navwalker::fallback',
        'walker'          => new bs4navwalker()
      ]);
    ?>
    </div>
  </nav>
  <div class="page-header jumbotron text-light bg-primary gradient-primary">
    <div class="container">
      <h1 class="my-5"><?= Titles\title(); ?></h1>
    </div>
  </div>
</header>