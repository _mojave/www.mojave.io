<footer class="content-info container">
    <div class="row">
      <?php
        wp_nav_menu([
          'menu'            => 'footer_navigation',
          'theme_location'  => 'footer_navigation',
          'menu_id'         => false,
          'menu_class'      => 'nav mx-auto',
          'depth'           => 2,
          'fallback_cb'     => 'bs4navwalker::fallback',
          'walker'          => new bs4navwalker()
        ]);
      ?>
    </div>
    <div class="row py-5">
      <?php dynamic_sidebar('footer-section-1'); ?>
      <?php dynamic_sidebar('footer-section-2'); ?>
      <?php dynamic_sidebar('footer-section-3'); ?>
      <?php dynamic_sidebar('footer-section-4'); ?>
    </div>
    <div class="row">
      <p>
        &copy;  <?php echo date('Y'); ?> | <?php bloginfo('name'); ?> 
      <br/>
        Aviary theme designed by Steven Roland
      </p>
    </div>
</footer>